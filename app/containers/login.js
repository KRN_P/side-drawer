import React, { Component } from "react";
import {
    Text,
    View
} from "react-native";
export default class Login extends Component {
    componentDidMount() {
        setTimeout(() => {
            this.props.navigation.navigate("Dashboard")
        }, 2000)
    }
    render() {
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <Text style={{ fontSize: 20, color: '#333' }}>
                    Login
                </Text>
            </View>
        )
    }
}