import React, { Component } from "react";
import {
    Text,
    View,
    TouchableWithoutFeedback
} from "react-native";
import { DrawerActions } from 'react-navigation';
export default class Dashboard extends Component {
    render() {
        const { navigation } = this.props
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <TouchableWithoutFeedback onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())}>
                    <View>
                        <Text style={{ fontSize: 20, color: '#333' }}>Open Menu</Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        )
    }
}