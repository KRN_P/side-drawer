import React, { Component } from "react";
import {
    Text,
    View
} from "react-native";
export default class SideDrawer extends Component {
    render() {
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <Text style={{ fontSize: 20, color: '#333' }}>
                    Drawer
                </Text>
            </View>
        )
    }
}