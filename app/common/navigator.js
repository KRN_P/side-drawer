import { Dimensions } from "react-native"
import { createStackNavigator, createAppContainer, createDrawerNavigator } from "react-navigation";
import DashboardComponent from "../containers/dashboard";
import LoginComponent from "../containers/login";
import SideDrawer from "./sidedrawer";
const routes = {
    Dashboard: DashboardComponent,

};
const options = {
    contentComponent: SideDrawer,
    drawerWidth: Dimensions.get('window').width - 119,
    drawerLockMode: 'locked-closed',
    style: {
        paddingTop: 0,
    },
};

const DrawerRoutes = createDrawerNavigator(routes, options);

const Navigator = createStackNavigator({
    Auth: LoginComponent,
    Drawer: DrawerRoutes,
},
    {
        initialRouteName: "Auth",
        headerMode: "none",
        mode: "modal"
    });
export default createAppContainer(Navigator);